const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
gulp.task('htmlmin', function() {
  return gulp
    .src('./src/index.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist/'));
});
gulp.task('default', ['htmlmin'], function() {
  gulp.watch('./src/index.html', ['htmlmin']);
});
